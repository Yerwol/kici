# KiCi - An opinionated KiCad/KiBot Continuous Integration pipeline
Running CI/CD for software is so common it could be considered a standard.
Doing the same for hardware is relative new. [KiCad] being a powerful EDA,
cannot yet fully do everything without outside help. To help here, there is
[KiBot], which tries to automate most tasks to work autonomous.

KiCi is an GitLab catalog component, that tries to simplify CI pipelines to
generate the most common outputs, from diffs in PDF form for reviews, to gerbers
for various common fab-houses. By no means tries KiCi to solve ever and all
KiCad automation problems, it just wants to help make things easier for the
most common cases. Some/most things can be overridden, but in the end, KiCi is
opinionated. But it can be happily used as a starting/learning point!


[TOC]


## Installation
To use KiCi, a few things are needed. First and foremost, the catalog needs to
be added to your pipeline. The built-in GitLab pipeline editor has easy ways to
find and use these components, but in the end it boils down to having the
following in the `.gitlab-ci.yml` file where `<VERSION>` is the desires tag,
branch or hash. Be careful with the `latest` version, as likewise to container
tags, this is the latest published version, which could be a bugfix to previous
release. Use `stable` if tracking the latest stable release is desired.

```yaml
include:
  - component: gitlab.com/ci-includes/kici/gitlab-component@<VERSION>
    inputs:
      stage_lint: 'lint'
```

> __Note:__ KiCi focuses uppon KiCad projects as a whole, not individual
> schematics or PCB files. Further more, only one KiCad project file per
> sub-directory is expected/allowed for now (this may change in the future).

### KiBot yaml configuration
KiBot needs configuration files to function. There's two choices, one being
the obvious one, write your own. However that's cumbersome and would just
repeat what's here already, which is option two. It is certainly possible to
copy the files of this repository, the downside is that the configuration files
wouldn't be updated, unless a new and up to date copy is made. Instead it is
recommended to add this repository as a `git submodule` (or `git worktree`).

```console
$ git submodule add https://gitlab.com/ci-includes/kici .kici
$ mkdir .kibot
$ cd .kibot
$ ln -s ../.kici/kibot_jobs/kici_*.kibot.yaml .
```
Alternatively, the clone could be done to any different path, and resolved with
symlinks, as is done in this example repository.

> __Note:__ This repository does some funky redirects with the config file,
> to make inclusion easier. Take very special note of `kici_common.kibot.yaml`
> and how it _actually_ links to the roots `kici_common.kibot.yaml`. Which
> basically allows for repository overrides, but are a bit difficult to see in
> _this_ repository. Instead, looking into the `hello/.kibot` directory should
> give a better idea. This can all be a little confusing at first.

A simplified view, with a kicad project in a subdirectory.
```
project
├── hello
│   ├── hello.kicad_pcb
│   ├── hello.kicad_pro
│   ├── hello.kicad_sch
│   └── .kibot
│      ├── kici_common.kibot.yaml -> ../../kici_common.kibot.yaml
│      ├── kici_drc.kibot.yaml -> ../../.kici/kibot_jobs/kici_drc.kibot.yaml
│      ├── kici_erc.kibot.yaml -> ../../.kici/kibot_jobs/kici_erc.kibot.yaml
│      └── kici_pdf.kibot.yaml -> ../../.kici/kibot_jobs/kici_pdf.kibot.yaml
├── kici_common.kibot.yaml (imports kibot_jobs/kici_common.kibot.yaml, other things)
├── .kici (submodule, copy, personal etc config files)
└── .gitlab-ci.yaml
```

Remember to checkout a specific version or release branch to pin to a version.
Updating should be as straight forward as pulling and committing the submodule.


> __Warning:__ Due to KiCad bug #6918, always including the provided filter is
> needed when using local 3D files (e.g. stored and referenced in the repo).


### Job selection
Only the jobs that have a KiBot configuration file are executed and therefore
naming of the files is important. The structure is generally
`kici_<jobname>.kibot.yaml`, and only for pre-defined jobs. For a full list see
the [gitlab-component](templates/gitlab-component.yml) template.

The pipeline will check the entire repository for
`**/.kibot/kici_<jobname>.kibot.yaml` files to determine which scripts are there
and thus which jobs need to be run. There is one unhandled edge-case to be
aware of, in that when a multi-project repository has a shared `.kibot`
directory in the root, but some of the files jobs are not used, the pipeline
will trigger the job (it has found the kibot yaml script), but will not
execute the script for any component, as the project does not have it.
This is just a visual side effect and can easily be resolved by removing the
unused script file. GitLab cannot detect files (rules:exist) over
submodules and thus the edge-case is rather small.

When using a submodule to hold the kibot scripts, `.kibot` still needs to be
created and symlinks to the desired jobs created, also to select the desired
jobs, but also due to gitlab-org/gitlab#438863, which prevents the pipeline
to detect files outside of the repository.


### Hierarchical scheets
Because KiBot technically works on individual files (board files or schematic
files), KiBot gets confused when running KiBot on a project with multiple
schematic (or board files) in a single directory. In this case, it produces a
warning, which in the case of CI/CD will cause things to error out. To avoid
this, the template will search for root sheet and thus will always use that as
argument to KiBot. For PCB files this is not an issue, as we stated only a
single project and thus a single PCB file per directory is allowed.


### Manual execution
It is possible to also run the pipeline steps locally, which can be useful to
configure things and set them up. For this, [docker] is needed. In the following
example, the `latest` stable version of KiBot is used. Due to some bugs and
features this repository relies on, `dev` is used internally for now. Be aware
of this.

Lets pull and start a shell the container, assuming we are in the root of our
repository.

```console
$ docker run \
    --env HOME='/tmp' \
    --interactive \
    --pull always \
    --rm \
    --tty \
    --user "$(id -u):$(id -g)" \
    --volume "$(pwd):/workspace"
    --workspace "/workspace" \
    'ghcr.io/inti-cmnb/kicad8_auto_full:dev' \
    '/bin/bash'
```

With the container running, we can do the usual KiBot things, but some setup is
needed. The [`gitlab-component.yml`](templates/gitlab-component.yml) file
also does this.

* Optionally change to the project sub-directory, `world` in this example.
* Source all variables defined in our configuration file
* Export any variables, as kibot won't find them otherwise.
* CI_JOB_NAME is exported, because the only job needing it, is this example info job.
* Run KiBot with the right flags! (remember to append --schematic file.kicad_sch if needed)

```console
KiBot $ cd 'world'
KiBot $ . '.kibot/kici_conf.env'
KiBot $ for _k in $(grep "^KICI_.*=.*$" '.kibot/kici_conf.env'); do export "${_k%%'='*}"; done
KiBot $ export CI_JOB_NAME='KiCi'
KiBot $ kibot --defs-from-env --stop-on-warnings --plot-config '.kibot/kici_info.kibot.yaml'
```

> __Note:__ To update any variables in the conf file, just sourcing the file
> will set the content of already exported variables, so the for loop is only
> needed once. New variables added of course do need the loop, to export them.


#### Navigate results
The `navigate_results` output is special, in that it works best if all jobs
have produced their outputs, as the job also creates pretty thumbnails.

More importantly, navigational output is generated even when the job supplying
that output is never run, because it is always imported. Tuning the output
of `navigate_results` is certainly possible, but as this is an opinionated
way of doing things, diverging from these 'standard' outputs requires a
customized `kici_navigate.kibot.yaml`.

To locally run all outputs as part of `navigate_results`, supply the
environment variable `RUN_BY_DEFAULT=true` either as an `export`ed shell
variable or via the `--define RUN_BY_DEFAULT=true` or
`-e RUN_BY_DEFAULT=true` argument. This will force KiBot to generate all
outputs.


### Runner tags
None of the KiCi jobs configure `tags`, but do depend on `docker` as the
[KiBot] container is used. Since the runner tag is very setup specific, either
allow the desired runners to pick up `any` job, or set the default tag, which
does mean that this introduces less-desirable behavior for any other jobs in
the pipeline.

When using the runner tag `docker` for example, this would look as such.
```yaml
default:
  tags:
    - docker
```


### Inputs
The following inputs are accepted for the component. See the description field
in the `templates/gitlab-component.yml` header.

> __Note:__ The pipeline can easily be used to run for multiple projects within
> a repository. The test projects in this repository, `hello` and `world` are
> a good example of the configurability and flexibility offered. One limitation
> however is that spaces are not allowed in directory names, as this is used as
> the project separator.


### Variables
The following variables are accepted for the component. However it is strongly
recommended to commit them in `.kibot/kici_conf.env`.

| Variable                       | Default value | Description |
| ------------------------------ | ------------- | ----------- |
| `DEBUG_TRACE_CI`               |            '' | When set, enables the shells -x and runs kibot in debug mode. Note, that this does leave large videos! |
| `KICI_BLENDER_ISO_X`           |           135 | X angle of the isometric view. |
| `KICI_BLENDER_ISO_Y`           |           180 | Y angle of the isometric view. |
| `KICI_BLENDER_ISO_Z`           |           195 | Z angle of the isometric view. |
| `KICI_BLENDER_NO_DENOISER`     |         false | Allows for overriding the denoiser, but only if hardware support was detected. |
| `KICI_BLENDER_SAMPLES`         |       5 or 75 | Number of rendering samples for blender. The high number is the default when without SSE4.1. |
| `KICI_CI_TOKEN`                |            '' | Mandatory token to be used when using `input:attach_diff`. Should be replaced by `CI_JOB_TOKEN` in the future, Use a personal access token for now. WARNING, be aware of the consequences of token leakage. |
| `KICI_PDF_COLOR_PCB_MASK`      |     #f2eda1ff | The color the soldermask is rendered in for the Top and Bottom views. |
| `KICI_PDF_COLOR_PCB_SILK`      |     #14332440 | The color the silkscreen is rendered in for the Top and Bottom views. |
| `KICI_PDF_SCALING`             |           1.5 | The scaling factor applied to PDF images. |
| `KICI_RENDER3D_BOTTOM_MOVE_X`  |             0 | Moves on the Up (postive) Down (negative) axis for 'bottom' view. |
| `KICI_RENDER3D_BOTTOM_MOVE_Y`  |             0 | Moves on the Right (postive) Left (negative) axis for 'bottom' view. |
| `KICI_RENDER3D_BOTTOM_ZOOM`    |             0 | Zoom level for 'bottom' view. |
| `KICI_RENDER3D_ISO_MOVE_X`     |             0 | Moves on the Up (postive) Down (negative) axis for isometric view. |
| `KICI_RENDER3D_ISO_MOVE_Y`     |             0 | Moves on the Right (postive) Left (negative) axis for isometric view. |
| `KICI_RENDER3D_ISO_ROTATE_X`   |            -5 | Rotates the isometric view over the X axis. |
| `KICI_RENDER3D_ISO_ROTATE_Y`   |             0 | Rotates the isometric view over the Y axis. |
| `KICI_RENDER3D_ISO_ROTATE_Z`   |             2 | Rotates the isometric view over the Z axis. |
| `KICI_RENDER3D_ISO_ZOOM`       |             0 | Zoom level for isometric view. |
| `KICI_RENDER3D_TOP_MOVE_X`     |             0 | Moves on the Up (postive) Down (negative) axis for 'top' view. |
| `KICI_RENDER3D_TOP_MOVE_Y`     |             0 | Moves on the Right (postive) Left (negative) axis for 'top' view. |
| `KICI_RENDER3D_TOP_ZOOM`       |             0 | Zoom level for 'top' view. |
| `KICI_RENDER3D_HEIGHT`         |          1024 | Best effort image width. |
| `KICI_RENDER3D_WIDTH`          |          1440 | Best effort image height. |
| `KICI_RENDER3D_BG_TRANSPARENT` |         false | Make background transparent, done by kibot post-op. |

> __Note:__ It is almost always required to tweak the view of the KiCad built-in
> 3D render viewer. It is best to zoom to the desired zoom level, and only then
> move/rotate. The isometric view is the only one that offers rotation, as the
> intent is to offer an 'angled' view.
> Even though there are defaults defined, they are just there to get started.
> They very most likely need some fine tuning to the PCB in question. Especially
> for the render3D ones, they almost always require at the least adjustment of
> the zoom levels. See the test examples to get a better idea.


> **WARNING:** While the `attach_diff` feature is VERY useful, especially for
> reviews, it does require a token with access to the API. Since there is no
> fine grained control over the API yet, a project, group or personal token is
> required. Be sure to trust anybody that can create a branch (e.g. developer
> access), as they can leak that key quite easily! Be warned!


### SSE4.1 support
Blender's denoise algorithm, required for pretty 3D pictures, really likes to
have SSE4.1 support. Some CI systems (emulated CPU's) or old hardware (AMD
Phenom II and before) however do not offer this. The pipeline tries to detect
this and disables the denoiser, and increases the number of samples,
significantly adding render time.


## Included examples
The included examples, or tests, are intended to show off a few things. For
example, how to add multiple projects while sharing some data (3D models,
kibot configuration). To make use of as many features of KiCad and KiBot, one
KiCad project is a through-hole project, while the other is a SMD project.
While they could even share the same schematic, the BOM makes that quite a bit
harder, true multi-PCB support would really require KiCad support.

To configure KiCi, as part of the per project `.kibot` directory, a file
`kici_conf.env` is needed. The file is just a key=pair shell environment file,
which gets sourced by the pipeline script. This configuration file is unique
per project, and can be shared with some smart import or symlinking, as done
in this repository.


## A note on the project License
It is important to realize, that the License only applies to the KiCi code and
templates. These are generally never 'shared' or 'released' to the public (but
may be in a public repository of course). Whatever is generated using KiCi,
has whatever License comes with the content, it is not affected by KiCi
whatsoever. No, it does not taint your electronics, no your schematics are not
infected by the GPL. Changes, as per license, only to the KiCi code need to be
shared, with those who request them and only if they where part of a release,
which is almost never the case. Any legal dept/lawyer that claims otherwise,
a license change can be discussed.


## Others using KiCi?
See our [wiki](https://gitlab.com/ci-includes/kici/-/wikis/home) for a few
users of this components, but there might be more!


[KiCad]: https://kicad.org
[KiBot]: https://kibot.readthedocs.io
[docker]: https://docker.com
